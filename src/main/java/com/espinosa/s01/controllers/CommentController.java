package com.espinosa.s01.controllers;

import com.espinosa.s01.models.Comment;
import com.espinosa.s01.services.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@CrossOrigin
public class CommentController {
    @Autowired
    CommentService commentService;

    @RequestMapping(value = "/posts/{id}/comment", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestHeader(value = "Authorization") String token, @PathVariable Long id, @RequestBody Comment comment) {
        return commentService.createComment(token, id, comment);
    }

    @RequestMapping(value = "/posts/{postId}/comments/{commentId}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updateComment(@RequestHeader(value = "Authorization") String token, @PathVariable Long postId, @PathVariable Long commentId, @RequestBody Comment comment) {
        return commentService.updateComment(token, postId, commentId, comment);
    }

    @RequestMapping(value = "/posts/{postId}/comments/{commentId}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteComment(@RequestHeader(value = "Authorization") String token, @PathVariable Long postId, @PathVariable Long commentId) {
        return commentService.deleteComment(token, postId, commentId);
    }

    @RequestMapping(value = "/posts/{postId}/comments")
    public ResponseEntity<Object> getComments(@PathVariable Long postId) {
        Set<Comment> comments = commentService.getComments(postId);
        return new ResponseEntity<>(comments, HttpStatus.OK);
    }




}
