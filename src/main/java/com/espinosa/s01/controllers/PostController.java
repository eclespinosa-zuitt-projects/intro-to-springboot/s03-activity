package com.espinosa.s01.controllers;

import com.espinosa.s01.models.Post;
import com.espinosa.s01.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class PostController {
    @Autowired
    PostService postService;

    @RequestMapping(value = "/posts", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestBody Post newPost, @RequestHeader(value = "Authorization") String token) {
        postService.createPost(newPost, token);
        return new ResponseEntity<>("New Post was created.", HttpStatus.CREATED);
    }

    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public ResponseEntity<Object> getPost() {
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }

    @RequestMapping(value = "/my-posts", method = RequestMethod.GET)
    public ResponseEntity<Object> getPost(@RequestHeader(value = "Authorization") String token) {
        return new ResponseEntity<>(postService.getUserPosts(token), HttpStatus.OK);
    }

    @RequestMapping(value = "/posts/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updatePost(@PathVariable Long id, @RequestBody Post updatedPost, @RequestHeader(value = "Authorization") String token) {
        return postService.updatePost(id, updatedPost, token);
    }

    @RequestMapping(value = "/posts/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deletePost(@PathVariable Long id, @RequestHeader(value = "Authorization") String token) {
        return postService.deletePost(id, token);
    }
}
