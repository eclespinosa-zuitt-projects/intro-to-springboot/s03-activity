package com.espinosa.s01.repositories;

import com.espinosa.s01.models.Comment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepository extends CrudRepository<Comment, Object> {
}
