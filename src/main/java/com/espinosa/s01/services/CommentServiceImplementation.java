package com.espinosa.s01.services;

import com.espinosa.s01.config.JwtToken;
import com.espinosa.s01.models.Comment;
import com.espinosa.s01.models.Post;
import com.espinosa.s01.models.User;
import com.espinosa.s01.repositories.CommentRepository;
import com.espinosa.s01.repositories.PostRepository;
import com.espinosa.s01.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class CommentServiceImplementation implements CommentService{
    @Autowired
    private CommentRepository commentRepo;

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private PostRepository postRepo;

    @Autowired
    JwtToken jwtToken;

    @Override
    public ResponseEntity<Object> createComment(String token, Long postId, Comment comment) {
        String username = jwtToken.getUsernameFromToken(token);
        User author = userRepo.findByUsername(username);
        Post post = postRepo.findById(postId).isPresent() ? postRepo.findById(postId).get() : null;

        if (post != null) {
            Comment newComment = new Comment(comment.getContent(), post, author);
            commentRepo.save(newComment);

            return new ResponseEntity<>("comment added", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("post missing", HttpStatus.FORBIDDEN);
        }


    }

    @Override
    public ResponseEntity<Object> updateComment(String token, Long postId, Long commentId, Comment comment) {
        Comment oldComment = commentRepo.findById(commentId).isPresent() ? commentRepo.findById(commentId).get() : null;
        String username = jwtToken.getUsernameFromToken(token);
        User author = userRepo.findByUsername(username);
        Post post = postRepo.findById(postId).isPresent() ? postRepo.findById(postId).get() : null;

        if (oldComment != null && author.getUsername().equals(username) &&  oldComment.getUser().getUsername().equals(username) && post != null) {
            oldComment.setContent(comment.getContent());
            commentRepo.save(oldComment);

            return new ResponseEntity<>("comment updated", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("unauthorized", HttpStatus.FORBIDDEN);
        }
    }

    @Override
    public ResponseEntity<Object> deleteComment(String token, Long postId, Long commentId) {
        String username = jwtToken.getUsernameFromToken(token);
        User author = userRepo.findByUsername(username);
        Comment comment = commentRepo.findById(commentId).isPresent() ? commentRepo.findById(commentId).get() : null;

        if (comment != null && comment.getUser().getUsername().equals(author.getUsername())) {
            commentRepo.delete(comment);

            return new ResponseEntity<>("comment deleted", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("unauthorized", HttpStatus.FORBIDDEN);
        }
    }

    @Override
    public Set<Comment> getComments(Long postId) {
        Post post = postRepo.findById(postId).isPresent() ? postRepo.findById(postId).get() : null;
        return post != null ? post.getComments() : null;
    }
}
