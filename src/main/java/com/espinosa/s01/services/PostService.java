package com.espinosa.s01.services;

import com.espinosa.s01.models.Post;
import org.springframework.http.ResponseEntity;

import java.util.Set;

public interface PostService {
    void createPost(Post newPost, String token);
    ResponseEntity<Object> updatePost(Long id, Post updatedPost, String token);
    ResponseEntity<Object> deletePost(Long id, String token);
    Iterable<Post> getPosts();
    Set<Post> getUserPosts(String token);

}
