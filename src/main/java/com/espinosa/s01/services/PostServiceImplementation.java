package com.espinosa.s01.services;

import com.espinosa.s01.config.JwtToken;
import com.espinosa.s01.models.Post;
import com.espinosa.s01.models.User;
import com.espinosa.s01.repositories.PostRepository;
import com.espinosa.s01.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class PostServiceImplementation implements PostService{
    @Autowired
    private PostRepository postRepo;

    @Autowired
    private UserRepository userRepo;

    @Autowired
    JwtToken jwtToken;

    @Override
    public void createPost(Post newPost, String token) {
        // Retrieve an existing user object
        User author = userRepo.findByUsername(jwtToken.getUsernameFromToken(token));
        System.out.println(token);
        Post newPostObj = new Post();
        newPostObj.setTitle(newPost.getTitle());
        newPostObj.setContent(newPost.getContent());
        newPostObj.setUser(author);

        postRepo.save(newPostObj);
    }

    @Override
    public ResponseEntity<Object> updatePost(Long id, Post updatedPost, String token) {
        // Retrieve the post to be updated
        Post existingPost = postRepo.findById(id).get();
        String authorUsername = existingPost.getUser().getUsername();
        String authenticatedUsername = jwtToken.getUsernameFromToken(token);

        if(authenticatedUsername.equals(authorUsername)) {
            existingPost.setTitle(updatedPost.getTitle());
            existingPost.setContent(updatedPost.getContent());

            postRepo.save(existingPost);

            return new ResponseEntity<>("An existing post was updated.", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You are not authorized to update this post", HttpStatus.UNAUTHORIZED);
        }
    }

    @Override
    public ResponseEntity<Object> deletePost(Long id, String token) {
        // Post post = postRepo.findById(id).get();
        Post existingPost = postRepo.findById(id).get();
        String authorUsername = existingPost.getUser().getUsername();
        String authenticatedUsername = jwtToken.getUsernameFromToken(token);

        if(authenticatedUsername.equals(authorUsername)) {
            postRepo.deleteById(id);

            return new ResponseEntity<>("An existing post was deleted.", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You are not authorized to delete this post", HttpStatus.UNAUTHORIZED);
        }
    }

    @Override
    public Iterable<Post> getPosts() {
        return postRepo.findAll();
    }

    @Override
    public Set<Post> getUserPosts(String token) {
        User author = userRepo.findByUsername(jwtToken.getUsernameFromToken(token));
        return author.getPosts();

//        return postRepo.findAllById;
    }
}
