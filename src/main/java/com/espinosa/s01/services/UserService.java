package com.espinosa.s01.services;


import com.espinosa.s01.models.User;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface UserService {
    void createUser(User user);
    ResponseEntity<Object> updateUser(Long id, String token, User updatedUser);
    void deleteUser(String token);
    User getUserInfo(String token);
    Iterable<User> getUsers(String token);
    Optional<User> findByUsername(String username);
}
