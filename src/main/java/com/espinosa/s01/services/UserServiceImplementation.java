package com.espinosa.s01.services;

import com.espinosa.s01.config.JwtToken;
import com.espinosa.s01.models.User;
import com.espinosa.s01.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImplementation implements UserService {
    @Autowired
    private UserRepository userRepo;

    @Autowired
    private JwtToken jwtToken;

    @Override
    public void createUser(User user) {
        userRepo.save(user);
    }

    @Override
    public ResponseEntity<Object> updateUser(Long id, String token, User updatedUser) {
        String username = jwtToken.getUsernameFromToken(token);
        User existingUser = userRepo.findById(id).get();

        if(username.equals(existingUser.getUsername())) {
            String hashedPassword = new BCryptPasswordEncoder().encode(updatedUser.getPassword());

            existingUser.setUsername(updatedUser.getUsername());
            existingUser.setPassword(hashedPassword);
            userRepo.save(existingUser);

            return new ResponseEntity<>("User updated.", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Action forbidden.", HttpStatus.FORBIDDEN);
        }
    }

    @Override
    public void deleteUser(String token) {
        String username = jwtToken.getUsernameFromToken(token);
        User user = userRepo.findByUsername(username);
        userRepo.deleteById(user.getId());
    }

    @Override
    public User getUserInfo(String token) {
        String username = jwtToken.getUsernameFromToken(token);
        return userRepo.findByUsername(username);
    }

    @Override
    public Iterable<User> getUsers(String token) {
        String username = jwtToken.getUsernameFromToken(token);
        boolean isExisting = userRepo.findByUsername(username) != null;

        return isExisting ? userRepo.findAll() : null;
    }

    @Override
    public Optional<User> findByUsername(String username) {
        return Optional.ofNullable(userRepo.findByUsername(username));
    }
}
